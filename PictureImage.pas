unit PictureImage;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, stdctrls;

type
  TPictureImage = class(TPanel)
  private
    { Private declarations }
  protected
    { Protected declarations }
    FImage: TImage;
    FLabel: TLabel;
//    function Get_Height: Integer;
//    procedure Set_Height(Value: Integer);
//    function Get_Width: Integer;
//    procedure Set_Width(Value: Integer);
    function Get_Caption: TCaption;
    procedure Set_Caption(Value: TCaption);
    procedure Resize; override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
//    property Height: Integer read Get_Height write Set_Height;
//    property Width: Integer read Get_Width write Set_Width;
    property Caption: TCaption read Get_Caption write Set_Caption;
    property Image: TImage read FImage;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Cartiva', [TPictureImage]);
end;

{ TPictureImage }

constructor TPictureImage.Create(AOwner: TComponent);
begin
  inherited;
  Self.Height := 50;
  Self.Width := 50;
  FImage := TImage.Create(Self);  //DEPRECATED  This is so that the TImage is owned by the form, permitting Form2.FindComponent to work
  FLabel := TLabel.Create(Self);
  FImage.Parent := Self;
  FLabel.Parent := Self;
  FLabel.Top := 2;
  FLabel.Left := 2;
  FImage.Top := 18;
  FImage.Left := 2;
//  FImage.SetSubComponent(True);
  FImage.Stretch := True;
end;

function TPictureImage.Get_Caption: TCaption;
begin
  Result := FLabel.Caption;
end;

procedure TPictureImage.Loaded;
begin
  inherited;
  FImage.Name := 'PicImage' + Copy(Self.Name, 13,3);
  FLabel.Name := 'PicLabel' + Copy(Self.Name, 13,3);
end;

//function TPictureImage.Get_Height: Integer;
//begin
//  Result := inherited Height;
//end;
//
//function TPictureImage.Get_Width: Integer;
//begin
//  Result := inherited Width
//end;

procedure TPictureImage.Resize;
begin
  inherited;
  Self.FImage.SetBounds(FImage.Left, FImage.Top, Self.Width - 4, Self.Height - 20);
end;

procedure TPictureImage.Set_Caption(Value: TCaption);
begin
  if Value <> FLabel.Caption then
    FLabel.Caption := Value;
end;

//procedure TPictureImage.Set_Height(Value: Integer);
//begin
//  if Value <> inherited Height then
//  begin
//    inherited Height := Value;
//    FImage.Height := Self.Height - 4;
//  end;
//end;
//
//procedure TPictureImage.Set_Width(Value: Integer);
//begin
//  if Value <> inherited Width then
//  begin
//    inherited Width := Value;
//    FImage.Width := Self.Width - 4;
//  end;
//end;

end.
