object ProgressForm: TProgressForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Progress'
  ClientHeight = 93
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 51
    Height = 13
    Alignment = taCenter
    Caption = 'Working...'
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 42
    Width = 330
    Height = 17
    Position = 100
    TabOrder = 0
  end
end
