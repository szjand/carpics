unit ConfirmUploadu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TConfirmUpload = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    Image1: TImage;
    CheckBox1: TCheckBox;
    VehicleDescription: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ShowModal(Msg: String): Integer; overload;
  end;

var
  ConfirmUpload: TConfirmUpload;

implementation

uses mainformu;

{$R *.dfm}

{ TUploadComplete }

procedure TConfirmUpload.Button1Click(Sender: TObject);
begin
  Form1.CleanupFiles := CheckBox1.Checked;
  Self.ModalResult := mrOK;
end;

function TConfirmUpload.ShowModal(Msg: String): Integer;
begin
  Label1.Caption := Msg;
  CheckBox1.Checked := Form1.CleanupFiles;
  CheckBox1.Caption := 'Delete files from ' + mainformu.DirName;
  Result := Self.ShowModal;
end;

end.
