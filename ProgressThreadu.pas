unit ProgressThreadu;

interface

uses
  Classes, ProgressFormu, SyncObjs;

type
  TProgressThread = class(TThread)
  private
    { Private declarations }
    FProgressForm: TProgressForm;
    FProgressEvent: TEvent;
    FCaption: String;
  protected
    procedure Execute; override;
  public
    constructor Create(CreateSuspended: Boolean; Caption: String; ProgressEvent: TEvent); overload;
  end;

implementation

uses mainformu;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TProgressThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TProgressThread }

constructor TProgressThread.Create(CreateSuspended: Boolean; Caption: String; ProgressEvent: TEvent);
begin
  inherited Create(True);
  FCaption := Caption;
  FProgressEvent := ProgressEvent;
  if not CreateSuspended then
    Resume;
end;

procedure TProgressThread.Execute;
var
  ProgressValue: Integer;
  RetVal: TWaitResult;
begin
  FProgressForm := TProgressForm.Create(nil);
  FProgressForm.Caption := 'CarPics is loading. Please wait...';
  FProgressForm.SetProgress(0);
  FProgressForm.Show;
  FProgressForm.BringToFront;
  try
    repeat
    begin
      RetVal := FProgressEvent.WaitFor(20000);
      if RetVal = wrTimeout then
        exit;
      ProgressValue := mainformu.ProgressFormProgress;
      if ProgressValue >= 100 then
        exit
      else
        FProgressForm.SetProgress(ProgressValue);
    end
    until False;
  finally
    FProgressForm.Release;
  end;
end;

end.
