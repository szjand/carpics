object Form1: TForm1
  Left = 22
  Top = 63
  Caption = 'Cartiva CarPics'
  ClientHeight = 741
  ClientWidth = 1117
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDragDrop = PicImageDragDrop
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel1: TGridPanel
    Left = 225
    Top = 29
    Width = 892
    Height = 693
    Align = alClient
    ColumnCollection = <
      item
        Value = 14.285714285714280000
      end
      item
        Value = 14.285714285714280000
      end
      item
        Value = 14.285714285714280000
      end
      item
        Value = 14.285714285714290000
      end
      item
        Value = 14.285714285714290000
      end
      item
        Value = 14.285714285714290000
      end
      item
        Value = 14.285714285714300000
      end>
    ControlCollection = <
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = 0
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = 1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = 2
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = -1
      end
      item
        Column = -1
        Row = 3
      end
      item
        Column = 0
        Control = PictureImage1
        Row = 0
      end
      item
        Column = 1
        Control = PictureImage2
        Row = 0
      end
      item
        Column = 2
        Control = PictureImage3
        Row = 0
      end
      item
        Column = 3
        Control = PictureImage4
        Row = 0
      end
      item
        Column = 4
        Control = PictureImage5
        Row = 0
      end
      item
        Column = 5
        Control = PictureImage6
        Row = 0
      end
      item
        Column = 6
        Control = PictureImage7
        Row = 0
      end
      item
        Column = 0
        Control = PictureImage8
        Row = 1
      end
      item
        Column = 1
        Control = PictureImage9
        Row = 1
      end
      item
        Column = 2
        Control = PictureImage10
        Row = 1
      end
      item
        Column = 3
        Control = PictureImage11
        Row = 1
      end
      item
        Column = 4
        Control = PictureImage12
        Row = 1
      end
      item
        Column = 5
        Control = PictureImage13
        Row = 1
      end
      item
        Column = 6
        Control = PictureImage14
        Row = 1
      end
      item
        Column = 0
        Control = PictureImage15
        Row = 2
      end
      item
        Column = 1
        Control = PictureImage16
        Row = 2
      end
      item
        Column = 2
        Control = PictureImage17
        Row = 2
      end
      item
        Column = 3
        Control = PictureImage18
        Row = 2
      end
      item
        Column = 4
        Control = PictureImage19
        Row = 2
      end
      item
        Column = 5
        Control = PictureImage20
        Row = 2
      end
      item
        Column = 6
        Control = PictureImage21
        Row = 2
      end
      item
        Column = 0
        Control = PictureImage22
        Row = 3
      end
      item
        Column = 1
        Control = PictureImage23
        Row = 3
      end
      item
        Column = 2
        Control = PictureImage24
        Row = 3
      end
      item
        Column = 3
        Control = PictureImage25
        Row = 3
      end
      item
        Column = 4
        Control = PictureImage26
        Row = 3
      end
      item
        Column = 5
        Control = PictureImage27
        Row = 3
      end
      item
        Column = 6
        Control = PictureImage28
        Row = 3
      end>
    RowCollection = <
      item
        Value = 25.000000000000000000
      end
      item
        Value = 25.000000000000000000
      end
      item
        Value = 25.000000000000000000
      end
      item
        Value = 25.000000000000000000
      end>
    TabOrder = 2
    object PictureImage1: TPictureImage
      Left = 1
      Top = 1
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel1'
      TabOrder = 0
      object BitBtn1: TBitBtn
        Left = -92
        Top = 56
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        DoubleBuffered = True
        ParentDoubleBuffered = False
        TabOrder = 0
      end
    end
    object PictureImage2: TPictureImage
      Left = 128
      Top = 1
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel2'
      TabOrder = 1
    end
    object PictureImage3: TPictureImage
      Left = 255
      Top = 1
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel3'
      TabOrder = 2
    end
    object PictureImage4: TPictureImage
      Left = 382
      Top = 1
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel4'
      TabOrder = 3
    end
    object PictureImage5: TPictureImage
      Left = 509
      Top = 1
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel5'
      TabOrder = 4
    end
    object PictureImage6: TPictureImage
      Left = 636
      Top = 1
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel6'
      TabOrder = 5
    end
    object PictureImage7: TPictureImage
      Left = 763
      Top = 1
      Width = 128
      Height = 172
      Align = alClient
      Caption = 'PicLabel7'
      TabOrder = 6
    end
    object PictureImage8: TPictureImage
      Left = 1
      Top = 173
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel8'
      TabOrder = 7
    end
    object PictureImage9: TPictureImage
      Left = 128
      Top = 173
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel9'
      TabOrder = 8
    end
    object PictureImage10: TPictureImage
      Left = 255
      Top = 173
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel10'
      TabOrder = 9
    end
    object PictureImage11: TPictureImage
      Left = 382
      Top = 173
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel11'
      TabOrder = 10
    end
    object PictureImage12: TPictureImage
      Left = 509
      Top = 173
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel12'
      TabOrder = 11
    end
    object PictureImage13: TPictureImage
      Left = 636
      Top = 173
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel13'
      TabOrder = 12
    end
    object PictureImage14: TPictureImage
      Left = 763
      Top = 173
      Width = 128
      Height = 172
      Align = alClient
      Caption = 'PicLabel14'
      TabOrder = 13
    end
    object PictureImage15: TPictureImage
      Left = 1
      Top = 345
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel15'
      TabOrder = 14
    end
    object PictureImage16: TPictureImage
      Left = 128
      Top = 345
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel16'
      TabOrder = 15
    end
    object PictureImage17: TPictureImage
      Left = 255
      Top = 345
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel17'
      TabOrder = 16
    end
    object PictureImage18: TPictureImage
      Left = 382
      Top = 345
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel18'
      TabOrder = 17
    end
    object PictureImage19: TPictureImage
      Left = 509
      Top = 345
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel19'
      TabOrder = 18
    end
    object PictureImage20: TPictureImage
      Left = 636
      Top = 345
      Width = 127
      Height = 172
      Align = alClient
      Caption = 'PicLabel20'
      TabOrder = 19
    end
    object PictureImage21: TPictureImage
      Left = 763
      Top = 345
      Width = 128
      Height = 172
      Align = alClient
      Caption = 'PicLabel21'
      TabOrder = 20
    end
    object PictureImage22: TPictureImage
      Left = 1
      Top = 517
      Width = 127
      Height = 175
      Align = alClient
      Caption = 'PicLabel22'
      TabOrder = 21
    end
    object PictureImage23: TPictureImage
      Left = 128
      Top = 517
      Width = 127
      Height = 175
      Align = alClient
      Caption = 'PicLabel23'
      TabOrder = 22
    end
    object PictureImage24: TPictureImage
      Left = 255
      Top = 517
      Width = 127
      Height = 175
      Align = alClient
      Caption = 'PicLabel24'
      TabOrder = 23
    end
    object PictureImage25: TPictureImage
      Left = 382
      Top = 517
      Width = 127
      Height = 175
      Align = alClient
      Caption = 'PicLabel25'
      TabOrder = 24
    end
    object PictureImage26: TPictureImage
      Left = 509
      Top = 517
      Width = 127
      Height = 175
      Align = alClient
      Caption = 'PicLabel26'
      TabOrder = 25
    end
    object PictureImage27: TPictureImage
      Left = 636
      Top = 517
      Width = 127
      Height = 175
      Align = alClient
      Caption = 'PicLabel27'
      TabOrder = 26
    end
    object PictureImage28: TPictureImage
      Left = 763
      Top = 517
      Width = 128
      Height = 175
      Align = alClient
      Caption = 'PicLabel28'
      TabOrder = 27
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1117
    Height = 29
    Align = alTop
    TabOrder = 0
    object Label35: TLabel
      Left = 11
      Top = 5
      Width = 80
      Height = 13
      Caption = 'Picture Directory'
    end
    object Button1: TButton
      Left = 448
      Top = 2
      Width = 138
      Height = 21
      Caption = 'Select Picture Directory'
      TabOrder = 0
      OnClick = Button1Click
    end
    object DirEdit: TEdit
      Left = 120
      Top = 2
      Width = 315
      Height = 21
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 29
    Width = 225
    Height = 693
    Align = alLeft
    TabOrder = 1
    DesignSize = (
      225
      693)
    object Label34: TLabel
      Left = 7
      Top = 97
      Width = 35
      Height = 13
      Caption = 'File List'
    end
    object VehicleDescriptionLabel: TLabel
      Left = 7
      Top = 31
      Width = 192
      Height = 39
      AutoSize = False
      Caption = 'No Vehicle Selected'
      WordWrap = True
    end
    object LoadedPictureLabel: TLabel
      Left = 7
      Top = 451
      Width = 3
      Height = 13
    end
    object ListBox1: TListBox
      Left = 8
      Top = 114
      Width = 211
      Height = 331
      Style = lbOwnerDrawVariable
      Anchors = [akLeft, akTop, akBottom]
      DragMode = dmAutomatic
      ItemHeight = 13
      TabOrder = 0
      OnDragDrop = RecycleBinDragDrop
      OnDragOver = RecycleBinDragOver
      OnDrawItem = ListBox1DrawItem
      OnKeyUp = ListBox1KeyUp
      OnMeasureItem = ListBox1MeasureItem
    end
    object Button2: TButton
      Left = 144
      Top = 451
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Load All >>'
      TabOrder = 1
      OnClick = Button2Click
    end
    object SelectVehicleButton: TButton
      Left = 3
      Top = 2
      Width = 115
      Height = 25
      Caption = 'Select Vehicle'
      TabOrder = 2
      OnClick = SelectVehicleButtonClick
    end
    object BitBtn2: TBitBtn
      Left = 105
      Top = 83
      Width = 27
      Height = 25
      Hint = 'Move picture up in list'
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
        3333333333777F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
        3333333777737777F333333099999990333333373F3333373333333309999903
        333333337F33337F33333333099999033333333373F333733333333330999033
        3333333337F337F3333333333099903333333333373F37333333333333090333
        33333333337F7F33333333333309033333333333337373333333333333303333
        333333333337F333333333333330333333333333333733333333}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 138
      Top = 83
      Width = 27
      Height = 25
      Hint = 'Move picture down in list'
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        333333333337F33333333333333033333333333333373F333333333333090333
        33333333337F7F33333333333309033333333333337373F33333333330999033
        3333333337F337F33333333330999033333333333733373F3333333309999903
        333333337F33337F33333333099999033333333373333373F333333099999990
        33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333309033333333333337F7F333333333333090333
        33333333337F7F33333333333300033333333333337773333333}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = BitBtn3Clic
    end
    object BitBtn4: TBitBtn
      Left = 171
      Top = 83
      Width = 27
      Height = 25
      Hint = 'Remove picture from list'
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
        555557777F777555F55500000000555055557777777755F75555005500055055
        555577F5777F57555555005550055555555577FF577F5FF55555500550050055
        5555577FF77577FF555555005050110555555577F757777FF555555505099910
        555555FF75777777FF555005550999910555577F5F77777775F5500505509990
        3055577F75F77777575F55005055090B030555775755777575755555555550B0
        B03055555F555757575755550555550B0B335555755555757555555555555550
        BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
        50BB555555555555575F555555555555550B5555555555555575}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = BitBtn4Click
    end
    object DiffPictureImage: TPictureImage
      Left = 11
      Top = 482
      Width = 198
      Height = 207
      Anchors = [akLeft, akRight, akBottom]
      Caption = 'Differentiator'
      PopupMenu = PopupMenu2
      TabOrder = 6
    end
    object BitBtn5: TBitBtn
      Left = 72
      Top = 83
      Width = 27
      Height = 25
      Hint = 'Reload picture list'
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555555555555555555555555555555555555555555555555555555555555
        555555555555555555555555555555555555555FFFFFFFFFF555550000000000
        55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
        B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
        000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
        555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
        55555575FFF75555555555700007555555555557777555555555555555555555
        5555555555555555555555555555555555555555555555555555}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = LoadorReloadFiles1Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 722
    Width = 1117
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object OpenDialog1: TOpenDialog
    Left = 764
    Top = 58
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 822
    Top = 57
    object DisplayImageFullSize1: TMenuItem
      Caption = 'Display Image Full Size'
      Visible = False
      OnClick = DisplayImageFullSize1Click
    end
    object RemoveImage1: TMenuItem
      Caption = 'De-select Image'
      Visible = False
      OnClick = RemoveImage1Click
    end
    object RemoveAllImages1: TMenuItem
      Caption = 'De-select All Images'
      Visible = False
      OnClick = RemoveAllImages1Click
    end
  end
  object MainMenu1: TMainMenu
    Left = 877
    Top = 57
    object File1: TMenuItem
      Caption = 'File'
      object LoadorReloadFiles1: TMenuItem
        Caption = 'Load (or Reload) Files'
        OnClick = LoadorReloadFiles1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object ClearAll1: TMenuItem
        Caption = 'Clear All'
        OnClick = ClearAll1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
        ShortCut = 16472
        OnClick = Exit1Click
      end
    end
    object Pictures1: TMenuItem
      Caption = 'Layout'
    end
    object Pictures2: TMenuItem
      Caption = 'Pictures'
      object Upload1: TMenuItem
        Caption = 'Upload'
        Enabled = False
        OnClick = Upload1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object DeletePicturesfromVehicle1: TMenuItem
        Caption = 'Delete Pictures from Vehicle'
        Enabled = False
        OnClick = DeletePicturesfromVehicle1Click
      end
      object N2: TMenuItem
        Caption = '-'
        Visible = False
      end
      object ManageRydellDifferentiatorPictures1: TMenuItem
        Caption = 'Manage Rydell Differentiator Pictures'
        Visible = False
        OnClick = ManageRydellDifferentiatorPictures1Click
      end
    end
  end
  object PictureImages: TImageList
    Height = 30
    Width = 40
    Left = 930
    Top = 56
  end
  object PopupMenu2: TPopupMenu
    Left = 992
    Top = 56
    object DisplayImageFullSize2: TMenuItem
      Caption = 'Display Image Full Size'
      OnClick = DisplayImageFullSize2Click
    end
  end
end
