{/////////////////////////

  This form is currently not in use

//////////////////////////}
unit Differentiatoru;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, StdCtrls, ExtCtrls, ExtDlgs;

type
  TDifferentiator = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    OpenPictureDialog1: TOpenPictureDialog;
    Image1: TImage;
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Differentiator: TDifferentiator;

implementation

uses datamodu;

{$R *.dfm}

procedure TDifferentiator.Button1Click(Sender: TObject);
begin
if OpenPictureDialog1.Execute then
begin
   Image1.Picture.LoadFromFile(OpenPictureDialog1.Filename);
end;
end;

procedure TDifferentiator.Button2Click(Sender: TObject);
begin
if Image1.Picture = nil then
begin
  ShowMessage('You must select an image to add');
  exit;
end;
if Edit1.Text = '' then
begin
  ShowMessage('Please provide a description of the picture');
  exit;
end;
if Length(Edit1.Text) > 40 then
begin
  ShowMessage('Image description cannot be longer than 40 characters');
  exit;
end;
end;

procedure TDifferentiator.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DataMOdule1.AdsQuery1.Close;
end;

procedure TDifferentiator.FormCreate(Sender: TObject);
begin
  DataModule1.AdsQuery1.Close;
  DataModule1.AdsQuery1.SQL.Text := 'SELECT * FROM ' + DataModule1.VehiclePictureTableName + 'RydellDifferentiator';
  DataSource1.DataSet := DataModule1.AdsQuery1;
end;

end.
